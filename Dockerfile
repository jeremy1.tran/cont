FROM golang:1.8-alpine
LABEL maintener="Tran Jeremy"
RUN mkdir /app 
COPY . /app/ 
WORKDIR /app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
# Create a user named appuser inside appuser group
RUN addgroup -S appgroup && adduser -S appuser -G appgroup
# Define appuser as user to execute container
USER appuser

CMD ["/app/main"]

EXPOSE 80
